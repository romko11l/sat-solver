use clap::{App, Arg};

use crate::dimacs::parser::parse_dimacs;
// use crate::dpll_solver::solver::Solver;
use crate::cdcl_solver::solver::Solver;

pub mod dimacs;
// pub mod dpll_solver;
pub mod cdcl_solver;
pub mod formula;

fn main() {
    let matches = App::new("Sat Solver")
        .author("Lytkin R. <lytkin.roma00@mail.ru>")
        .arg(
            Arg::with_name("cnf-file")
                .short('c')
                .long("cnf-file")
                .value_name("FILE")
                .help("Path to file with dimacs-encoded CNF")
                .default_value("./examples/example.cnf")
                .takes_value(true),
        )
        .get_matches();
    let dimacs_file = matches
        .value_of("cnf-file")
        .unwrap_or("./examples/example.cnf");
    match parse_dimacs(dimacs_file) {
        Ok(cnf) => println!("{:?}", Solver::new(cnf).solve()),
        Err(e) => println!("Error: {}", e),
    }
}

#[cfg(test)]
mod tests {
    use std::fs;

    use crate::cdcl_solver::solver::{SatResult, Solver};
    use crate::dimacs::parser::parse_dimacs;

    #[test]
    fn sat_test() {
        let base = "./examples/bench1/sat/";
        let paths = fs::read_dir(base).unwrap();

        for path in paths {
            match parse_dimacs(
                path.unwrap()
                    .path()
                    .into_os_string()
                    .into_string()
                    .unwrap()
                    .as_str(),
            ) {
                Ok(cnf) => match Solver::new(cnf).solve() {
                    SatResult::Sat(_) => {}
                    SatResult::Unsat => {
                        panic!("Incorrect solution: get Unsat, expected Sat");
                    }
                },
                Err(e) => {
                    panic!("Can't parse file: {:?}", e);
                }
            }
        }
    }

    #[test]
    fn unsat_test() {
        let base = "./examples/bench1/unsat/";
        let paths = fs::read_dir(base).unwrap();

        for path in paths {
            match parse_dimacs(
                path.unwrap()
                    .path()
                    .into_os_string()
                    .into_string()
                    .unwrap()
                    .as_str(),
            ) {
                Ok(cnf) => match Solver::new(cnf).solve() {
                    SatResult::Sat(_) => {
                        panic!("Incorrect solution: get Sat, expected Unsat");
                    }
                    SatResult::Unsat => {}
                },
                Err(e) => {
                    panic!("Can't parse file: {:?}", e);
                }
            }
        }
    }
}
