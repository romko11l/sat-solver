use std::collections::{HashMap, HashSet};
use thiserror::Error;

use crate::formula::cnf::{Clause, Cnf};

#[derive(Error, Debug)]
pub enum SolverError {
    #[error("can`t pick branching variable")]
    PickBranchingVariableError,
    #[error("node with var `{0}` not found in implication graph")]
    NodeNotFoundError(u32),
}

#[derive(Debug, Clone, Hash, PartialEq, Eq)]
pub struct Node {
    pub var: u32,
    pub val: bool,
    pub decision_level: i32,
    pub ancestors_nums: Vec<usize>,
}

impl Node {
    pub fn new(var: u32, val: bool, decision_level: i32, ancestors_nums: Vec<usize>) -> Node {
        return Node {
            var: var,
            val: val,
            decision_level: decision_level,
            ancestors_nums: ancestors_nums,
        };
    }

    pub fn add_ancestor_num(&mut self, ancestor_node_num: usize) {
        self.ancestors_nums.push(ancestor_node_num);
    }
}

#[derive(Debug)]
pub struct ImplicationGraph {
    pub nodes_list: Vec<Node>,
}

impl ImplicationGraph {
    pub fn new() -> ImplicationGraph {
        return ImplicationGraph {
            nodes_list: Vec::new(),
        };
    }

    pub fn add_node(&mut self, node: Node) {
        self.nodes_list.push(node);
    }

    pub fn get_node_with_var(&self, var: u32) -> Result<&Node, SolverError> {
        for node in &self.nodes_list {
            if node.var == var {
                return Ok(node);
            }
        }
        return Err(SolverError::NodeNotFoundError(var));
    }

    pub fn get_node_num_with_var(&self, var: u32) -> Result<usize, SolverError> {
        for i in 0..self.nodes_list.len() {
            if self.nodes_list[i].var == var {
                return Ok(i);
            }
        }
        return Err(SolverError::NodeNotFoundError(var));
    }
}

#[derive(Debug)]
pub enum SatResult {
    Sat(HashMap<u32, VarValue>),
    Unsat,
}

#[derive(Debug, Clone)]
pub enum VarValue {
    Assign(bool),
    Unassign,
}

#[derive(Debug)]
pub enum ClauseType {
    Resolved(bool), // смогли вычислить дизъюнкт
    Unit(i32),      // чтобы дизъюнкт вычислялся в 1, нужно чтобы литерал i32 вычислялся в 1
    Unresolved,     // >= 2 литералов нельзя вычислить (не означены переменные в них)
}

#[derive(Debug)]
pub struct Solver {
    pub cnf: Cnf,
    pub vars: HashMap<u32, VarValue>,
    pub impl_graph: ImplicationGraph,
    pub decision_level: i32,
}

impl Solver {
    pub fn new(cnf: Cnf) -> Solver {
        let mut vars: HashMap<u32, VarValue> = HashMap::new();
        for clause in &cnf.clauses {
            for lit in &clause.literals {
                vars.insert(lit.wrapping_abs() as u32, VarValue::Unassign);
            }
        }
        return Solver {
            cnf: cnf,
            vars: vars,
            impl_graph: ImplicationGraph::new(),
            decision_level: 0,
        };
    }

    pub fn all_variables_assigned(&self) -> bool {
        for val in self.vars.values() {
            match val {
                VarValue::Unassign => return false,
                _ => {}
            }
        }
        return true;
    }

    pub fn get_clause_type(&self, clause: &Clause) -> ClauseType {
        let mut number_of_unassign_lit: u32 = 0;
        let mut search_lit: i32 = 0;
        let mut temp: bool;
        for lit in &clause.literals {
            match self.vars.get(&(lit.wrapping_abs() as u32)).unwrap() {
                VarValue::Assign(value) => {
                    if *lit < 0 {
                        temp = !(*value);
                    } else {
                        temp = *value;
                    }
                    if temp {
                        return ClauseType::Resolved(true);
                    }
                }
                VarValue::Unassign => {
                    number_of_unassign_lit += 1;
                    search_lit = *lit;
                }
            }
        }
        if number_of_unassign_lit >= 2 {
            return ClauseType::Unresolved;
        } else if number_of_unassign_lit == 1 {
            return ClauseType::Unit(search_lit);
        }
        return ClauseType::Resolved(false);
    }

    pub fn unit_propagation_conflict(&mut self) -> bool {
        let mut continue_loop: bool;
        loop {
            continue_loop = false;
            for clause in &self.cnf.clauses {
                match self.get_clause_type(clause) {
                    ClauseType::Resolved(res) => {
                        if !res {
                            return true; // нашли конфликт
                        }
                    }
                    ClauseType::Unit(lit) => {
                        let mut ancestors_num: Vec<usize> = Vec::new();
                        for l in &clause.literals {
                            if *l != lit {
                                match self
                                    .impl_graph
                                    .get_node_num_with_var(l.wrapping_abs() as u32)
                                {
                                    Ok(num) => ancestors_num.push(num),
                                    Err(err) => panic!("{:?}", err),
                                }
                            }
                        }
                        if lit < 0 {
                            self.vars
                                .insert(lit.wrapping_abs() as u32, VarValue::Assign(false));
                            self.impl_graph.add_node(Node::new(
                                lit.wrapping_abs() as u32,
                                false,
                                self.decision_level,
                                ancestors_num,
                            ));
                        } else {
                            self.vars
                                .insert(lit.wrapping_abs() as u32, VarValue::Assign(true));
                            self.impl_graph.add_node(Node::new(
                                lit.wrapping_abs() as u32,
                                true,
                                self.decision_level,
                                ancestors_num,
                            ));
                        }
                        continue_loop = true;
                        break;
                    }
                    ClauseType::Unresolved => {}
                }
            }
            if !continue_loop {
                return false; // за итерацию по clause-ам ничего не изменилось - выходим из цикла
            }
        }
    }

    pub fn pick_branching_variable(&mut self) -> Result<(), SolverError> {
        let mut selected_variable: u32 = 0;
        for (var, value) in &self.vars {
            match value {
                VarValue::Assign(_) => {}
                VarValue::Unassign => {
                    selected_variable = *var;
                    break;
                }
            }
        }
        if selected_variable == 0 {
            return Err(SolverError::PickBranchingVariableError);
        }
        self.decision_level += 1;
        self.vars.insert(selected_variable, VarValue::Assign(false));
        let var_node = Node::new(selected_variable, false, self.decision_level, Vec::new());
        self.impl_graph.add_node(var_node);
        return Ok(());
    }

    pub fn determined_pick_branching_variable(&mut self) -> Result<(), SolverError> {
        for clause in &self.cnf.clauses {
            for lit in &clause.literals {
                match self.vars.get(&(lit.wrapping_abs() as u32)).unwrap() {
                    VarValue::Assign(_) => {}
                    VarValue::Unassign => {
                        let val: bool;
                        if *lit < 0 {
                            val = false;
                        } else {
                            val = true;
                        }
                        self.decision_level += 1;
                        self.vars
                            .insert(lit.wrapping_abs() as u32, VarValue::Assign(val));
                        let var_node = Node::new(
                            lit.wrapping_abs() as u32,
                            val,
                            self.decision_level,
                            Vec::new(),
                        );
                        self.impl_graph.add_node(var_node);
                        return Ok(());
                    }
                }
            }
        }
        return Err(SolverError::PickBranchingVariableError);
    }

    pub fn get_clause(&self) -> Clause {
        let mut res: Clause = Clause::new();
        for (var, value) in &self.vars {
            match value {
                VarValue::Assign(val) => {
                    if *val {
                        res.add_literal(-(*var as i32));
                    } else {
                        res.add_literal(*var as i32);
                    }
                }
                VarValue::Unassign => {}
            }
        }
        return res;
    }

    pub fn conflict_analysis(&mut self) -> i32 {
        if self.decision_level == 0 {
            return -1;
        };
        let mut level_set: HashSet<i32> = HashSet::new();
        let mut node_set: HashSet<Node> = HashSet::new();
        for node in self.impl_graph.nodes_list.iter().rev() {
            if (node.decision_level == self.decision_level) && (node.ancestors_nums.len() > 0) {
                for i in &node.ancestors_nums {
                    if (self.impl_graph.nodes_list[*i].decision_level < self.decision_level)
                        || (self.impl_graph.nodes_list[*i].decision_level == self.decision_level
                            && self.impl_graph.nodes_list[*i].ancestors_nums.len() == 0)
                    {
                        level_set.insert(self.impl_graph.nodes_list[*i].decision_level);
                        node_set.insert(self.impl_graph.nodes_list[*i].clone());
                    }
                }
            }
            if (node.decision_level == self.decision_level) && (node.ancestors_nums.len() == 0) {
                level_set.insert(node.decision_level);
                node_set.insert(node.clone());
            }
            if node.decision_level < self.decision_level {
                break;
            }
        }
        let mut additionnal_clause: Clause = Clause::new();
        for node in node_set {
            let additionnal_lit: i32;
            if node.val {
                additionnal_lit = -(node.var as i32);
            } else {
                additionnal_lit = node.var as i32
            }
            additionnal_clause.add_literal(additionnal_lit);
        }
        self.cnf.add_clause(additionnal_clause);
        let min_level = level_set.into_iter().min().unwrap();
        return min_level;
    }

    pub fn back_track(&mut self) {
        let mut num: usize = 0;
        if self.decision_level > 0 {
            for i in 0..self.impl_graph.nodes_list.len() {
                if self.impl_graph.nodes_list[i].decision_level == self.decision_level + 1 {
                    num = i;
                    break;
                }
            }
            for i in num..self.impl_graph.nodes_list.len() {
                self.vars
                    .insert(self.impl_graph.nodes_list[i].var, VarValue::Unassign);
            }
            self.impl_graph
                .nodes_list
                .resize(num, Node::new(0, false, 0, Vec::new()));
        } else {
            self.impl_graph.nodes_list.clear();
            for clause in &self.cnf.clauses {
                for lit in &clause.literals {
                    self.vars
                        .insert(lit.wrapping_abs() as u32, VarValue::Unassign);
                }
            }
        }
    }

    pub fn solve(&mut self) -> SatResult {
        if self.unit_propagation_conflict() {
            return SatResult::Unsat;
        }
        while !self.all_variables_assigned() {
            if self.unit_propagation_conflict() {
                return SatResult::Unsat;
            }
            match self.determined_pick_branching_variable() {
                Ok(_) => {}
                Err(err) => {
                    panic!("{:?}", err)
                }
            }
            while self.unit_propagation_conflict() {
                let dl = self.conflict_analysis();
                if dl <= 0 {
                    return SatResult::Unsat;
                } else {
                    self.decision_level = dl - 1;
                    self.back_track();
                }
            }
        }
        return SatResult::Sat(self.vars.clone());
    }
}
