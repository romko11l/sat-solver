use thiserror::Error;

#[derive(Error, Debug)]
pub enum FormulaError {
    #[error("can`t add `{0}` literal into clause")]
    AddLitError(i32),
}

#[derive(Debug, Clone)]
pub struct Clause {
    pub literals: Vec<i32>,
}

impl Clause {
    pub fn new() -> Clause {
        Clause {
            literals: Vec::new(),
        }
    }

    pub fn len(&self) -> usize {
        self.literals.len()
    }

    pub fn add_literal(&mut self, lit: i32) {
        if lit == 0 {
            return;
        }
        self.literals.push(lit);
    }
}

#[derive(Debug, Clone)]
pub struct Cnf {
    pub clauses: Vec<Clause>,
}

impl Cnf {
    pub fn new() -> Cnf {
        return Cnf {
            clauses: Vec::new(),
        };
    }

    pub fn len(&self) -> usize {
        return self.clauses.len();
    }

    pub fn add_clause(&mut self, disj: Clause) {
        self.clauses.push(disj)
    }

    pub fn union(&self, cnf: &Cnf) -> Cnf {
        let mut res = Cnf::new();
        for clause in &self.clauses {
            res.add_clause(clause.clone())
        }
        for clause in &cnf.clauses {
            res.add_clause(clause.clone())
        }
        return res;
    }
}
