use std::collections::{HashMap, HashSet};

use crate::formula::cnf::{Clause, Cnf};

#[derive(Debug)]
pub enum SatResult {
    Sat(HashMap<u32, bool>),
    Unsat,
}

#[derive(Debug)]
pub enum ComputeResult {
    Assign(bool),
    Unassign,
}

#[derive(Debug)]
pub struct Solver {
    pub cnf: Cnf,
    pub vars: Vec<u32>,
    pub assigned_vars: HashMap<u32, bool>,
    pub additional_clauses: Cnf,
}

impl Solver {
    pub fn new(cnf: Cnf) -> Solver {
        let mut set_vars: HashSet<u32> = HashSet::new();
        for clause in &cnf.clauses {
            for lit in &clause.literals {
                set_vars.insert(lit.wrapping_abs() as u32);
            }
        }
        return Solver {
            cnf: cnf,
            vars: Vec::from_iter(set_vars),
            assigned_vars: HashMap::new(),
            additional_clauses: Cnf::new(),
        };
    }

    pub fn compute_clause(&self, clause: &Clause) -> ComputeResult {
        let mut res: ComputeResult = ComputeResult::Assign(false);
        let mut temp: bool;
        for lit in &clause.literals {
            match self.assigned_vars.get(&(lit.wrapping_abs() as u32)) {
                Some(val) => {
                    if *lit < 0 {
                        temp = !(*val);
                    } else {
                        temp = *val;
                    }
                    if temp {
                        return ComputeResult::Assign(true);
                    }
                }
                None => res = ComputeResult::Unassign,
            }
        }
        return res;
    }

    pub fn check_cnf(&self) -> ComputeResult {
        let mut res: ComputeResult = ComputeResult::Assign(true);
        for clause in self.cnf.union(&self.additional_clauses).clauses {
            match self.compute_clause(&clause) {
                ComputeResult::Assign(val) => {
                    if val {
                        continue;
                    } else {
                        return ComputeResult::Assign(false);
                    }
                }
                ComputeResult::Unassign => {
                    res = ComputeResult::Unassign;
                }
            }
        }
        return res;
    }

    pub fn naive_recursive_unit_propagation(&mut self, current_var_num: usize) -> bool {
        self.assigned_vars.insert(self.vars[current_var_num], false);
        let mut found_solution: bool;
        match self.check_cnf() {
            ComputeResult::Assign(val) => found_solution = val,
            ComputeResult::Unassign => {
                found_solution = self.naive_recursive_unit_propagation(current_var_num + 1)
            }
        }
        if found_solution {
            return true;
        }
        self.assigned_vars.insert(self.vars[current_var_num], true);
        match self.check_cnf() {
            ComputeResult::Assign(val) => found_solution = val,
            ComputeResult::Unassign => {
                found_solution = self.naive_recursive_unit_propagation(current_var_num + 1)
            }
        }
        if found_solution {
            return true;
        }
        self.assigned_vars.remove(&self.vars[current_var_num]);
        return false;
    }

    pub fn solve(&mut self) -> SatResult {
        if self.naive_recursive_unit_propagation(0) {
            return SatResult::Sat(self.assigned_vars.clone());
        }
        return SatResult::Unsat;
    }
}
