use std::fs;
use thiserror::Error;

use crate::formula::cnf::{Clause, Cnf, FormulaError};

#[derive(Error, Debug)]
pub enum ParserError {
    #[error("can`t get correct cnf from file")]
    ParseCnfFileError,
    #[error(transparent)]
    IOError(#[from] std::io::Error),
    #[error(transparent)]
    NumParseError(#[from] std::num::ParseIntError),
    #[error(transparent)]
    DisjunctionError(#[from] FormulaError),
}

pub fn parse_dimacs(file_path: &str) -> Result<Cnf, ParserError> {
    let contents = fs::read_to_string(file_path)?;
    let split = contents.trim().split('\n').collect::<Vec<&str>>();
    if split.len() < 3 {
        return Err(ParserError::ParseCnfFileError);
    }
    let header = split[1];
    let header_split = header.split(' ').collect::<Vec<&str>>();
    let var_num = header_split[2].parse::<i32>()?;
    let clauses_num = header_split[3].parse::<i32>()?;

    let mut cnf = Cnf::new();
    for line in split[2..].iter() {
        let disj_vec = line.split_whitespace().collect::<Vec<&str>>();
        let mut disj = Clause::new();
        for var in disj_vec {
            disj.add_literal(var.parse::<i32>()?);
        }
        cnf.add_clause(disj)
    }

    return Ok(cnf);
}
