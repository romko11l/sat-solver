.PHONY: build
build:
	cargo build --release --verbose

.PHONY: test
test:
	cargo test --verbose

.PHONY: lint
lint:
	cargo clippy

.PHONY: run
run:
	cargo run
